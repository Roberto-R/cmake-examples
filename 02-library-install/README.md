# Library Install #

This folder contains two projects: a custom library and a custom executable that uses that library. The library can be installed on the system, and the executable finds it using `find_package`.

## Build ##

Build the library:

```
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=~/cmake-examples/local
make
make install
```

In the commands above a different installation directory is specified, which is at the root of this repository. The `include` and `lib` directories will be created inside that directory.

To then build the executable:

```
mkdir build && cd build
cmake .. -DCMAKE_PREFIX_PATH=~/cmake-examples/local
make
```

Since we installed the library at a non-standard location, we need to add that path to our search paths. Without this addition, cmake gives an error saying 'mylibrary' could not be found.
