# Basics #

This folder contains a very basic hello-world application, without extra dependencies.

## Build ##

Build the application with the typical:

```
mkdir build && cd build
cmake ..
make
```
